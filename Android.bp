// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    default_applicable_licenses: ["Android-Apache-2.0"],
}

android_app {
    name: "BuiltInPrintService",
    certificate: "platform",
    privileged: true,
    srcs: [
        "src/**/*.java",
        "src/**/I*.aidl",
        "src/**/*.kt",
    ],
    static_libs: [
        "androidx.core_core",
        "bips_aconfig_flags_java_lib",
        "androidx.appcompat_appcompat",
        "androidx-constraintlayout_constraintlayout",
        "androidx.recyclerview_recyclerview",
        "androidx.preference_preference",
        "androidx.fragment_fragment",
        "androidx.lifecycle_lifecycle-common",
        "androidx.lifecycle_lifecycle-runtime",
        "androidx.lifecycle_lifecycle-livedata",
        "androidx.lifecycle_lifecycle-viewmodel-ktx",
        "androidx.lifecycle_lifecycle-livedata-ktx",
        "androidx.lifecycle_lifecycle-extensions",
        "androidx.annotation_annotation",
        "kotlin-stdlib",
        "kotlinx_coroutines",
        "kotlinx-coroutines-android",
        "androidx.core_core-ktx",
    ],
    flags_packages: [
        "bips_aconfig_flags",
    ],
    resource_dirs: ["res"],
    sdk_version: "system_current",
    optimize: {
        proguard_flags_files: ["proguard.flags"],
        shrink_resources: true,
        optimize: true,
    },
    jni_libs: [
        "libwfds",
        "libcups",
    ],
}
